amqp==2.2.2
Babel==2.5.3
beautifulsoup4==4.6.0
billiard==3.5.0.3
bleach==2.1.2
boto==2.48.0
celery==4.1.0
certifi==2018.1.18
chardet==3.0.4
contextlib2==0.5.5
coreapi==2.3.3
coreschema==0.0.4
createsend==4.2.7
defusedxml==0.5.0
diff-match-patch==20121119
Django<2.1
django-admin-sortable2==0.6.19
django-admin-view-permission==1.4
django-admin-views==0.8.0
-e git+https://github.com/saxix/django-adminactions@ef32750d82250490c7ef95a2efbc531b514618ac#egg=django_adminactions
-e git+https://github.com/PetrDlouhy/django-adminfilters.git@981d64dec5a653e6afba54a44adc65f7d653367e#egg=django_adminfilters
-e git+https://github.com/PetrDlouhy/django-advanced-filters.git@develop#egg=django_advanced_filters
django-appconf==1.0.2
django-author==1.0.2
django-betterforms==1.1.4
django-bleach==0.3.0
django-bootstrap3==9.1.0
-e git+https://github.com/PetrDlouhy/django-braces.git@2.0#egg=django-braces
django-bulk-update==2.2.0
django-cachalot==1.5.0
django-celery-beat==1.1.0
django-celery-email==2.0.0
django-chart-tools==1.0
django-class-based-auth-views==0.4
django-composite-field==0.9.0
django-cors-headers==2.1.0
django-crispy-forms==1.7.0
django-datatables-view==1.14.0
django-daterange-filter==1.3.0
django-dbbackup==3.2.0
django-denorm==1.0.0
django-extensions==1.9.9
django-extra-views==0.9.0
django-favicon==0.1.3
django-fieldsignals==0.3.0
django-fm==0.2.5
django-google-tag-manager==0.0.3
django-gpxpy==0.0.2
django-import-export==0.7.0
django-initial-field==0.1.1
django-ipware==2.0.1
django-isnull-list-filter==0.1.0
django-leaflet==0.23.0
-e git+https://github.com/PetrDlouhy/django-likes.git@ffe8a2b421415d0e1ff29dc84b3db997dd3f1e24#egg=django_likes
-e git+https://github.com/burke-software/django-mass-edit.git@92467f1ec4b2b7fd11615d311ba747e8cc5472ed#egg=django_mass_edit
django-model-utils==3.1.1
-e git+https://github.com/abdolhosein/django-modeltranslation@patch-the-issue-with-django2#egg=django-modeltranslation
django-nested-admin==3.0.21
django-oauth-toolkit==1.0.0
django-polymorphic==2.0
django-price-level==1.0.0
django-rank-query==0.1.0
django-redis==4.8.0
django-registration-redux==2.1
django-related-admin==0.5.0
django-rosetta==0.8.1
django-scribbler-django2.0
django-secretballot==1.0.0
-e git+https://github.com/PetrDlouhy/django-selectable.git@mergeup#egg=django_selectable
-e git+https://github.com/PetrDlouhy/django-sitetree#egg=django-sitetree
django-ses==0.8.5
django-settings-context-processor==0.2
django-smart-selects==1.5.3
django-softhyphen==1.1.0
-e git+https://github.com/PetrDlouhy/django-stdnumfield.git@mergeup#egg=django_stdnumfield  # git version is for exception_text parameter
django-storages==1.6.5
django-su==0.6.0
-e git+https://github.com/PetrDlouhy/django-subdomains@django20#egg=django-subdomains  # for Django 2.0 fix
-e git+https://github.com/PetrDlouhy/DjangoTableSelectMultipleWidget.git@1096c65f3dbc7c5112e20205d1d7e5b3f32a1b24#egg=django_table_select_widget  # for datatable_options parameter
-e git+https://github.com/adilkhash/django-wysiwyg-redactor@64d808ef58dfe23e476da2225cd3b83ff4d2cb7b#egg=django_wysiwyg_redactor
djangorestframework==3.7.7
djangorestframework-gis==0.12
djcacheutils==3.0.0
easy-thumbnails==2.5
et-xmlfile==1.0.1
fiobank==1.2.0
flower==0.9.2
gpxpy==1.1.2
gunicorn==19.7.1
html5lib==1.0.1
idna==2.6
-e git+https://github.com/PetrDlouhy/InvoiceGenerator.git@3e92674d81c92c3411cda162ae9116ca623b2344#egg=InvoiceGenerator
itypes==1.1.0
jdcal==1.3
Jinja2==2.10
kombu==4.1.0
lxml==4.1.1
MarkupSafe==1.0
microsofttranslator==0.8
newrelic==2.100.0.84
oauthlib==2.0.6
odfpy==1.3.6
olefile==0.44
openpyxl==2.4.10
ordereddict==1.1
Pillow==5.0.0
polib==1.1.0
psycopg2==2.7.3.2
PyJWT==1.5.3
PyPDF2==1.26.0
python-memcached==1.59
python-stdnum==1.5
python3-openid==3.1.0
pytz==2017.3
PyYAML==3.12
qrcode==5.3
qrplatba==0.3.4
raven==6.5.0
redis==2.10.6
reportlab==3.4.0
requests==2.18.4
requests-oauthlib==0.8.0
setuptools-git==1.2
simplejson==3.6.5
six==1.11.0
-e git+https://github.com/PetrDlouhy/slumber.git@6327c9d58bc9b832277295af0b4ea2ea5a9c2324#egg=slumber
social-auth-app-django==2.1.0
social-auth-core==1.6.0
SQLAlchemy==1.2.1
sqlparse==0.2.4
-e git+https://github.com/aquavitae/svg2rlg.git@0ba12ebc519597b02999cf7938eacf5eee344ebb#egg=svg2rlg
tablib==0.12.1
tornado==4.5.3
unicodecsv==0.14.1
Unidecode==1.0.22
uritemplate==3.0.0
urllib3==1.22
vine==1.1.4
webencodings==0.5.1
whitenoise==3.3.1
xlrd==1.1.0
xlwt==1.3.0
